CREATE DATABASE student_managment;
CREATE TABLE `student_managment`.`students` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(200) NOT NULL,
  `last_name` VARCHAR(200) NOT NULL,
  `average_grade` DOUBLE NOT NULL,
  `index_number` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `student_managment`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(200) NOT NULL,
  `password` VARCHAR(200) NOT NULL,  
  PRIMARY KEY (`id`));


INSERT INTO `student_managment`.`users` (`id`, `username`, `password`)
VALUES ('1', 'bojan', '1403730359');