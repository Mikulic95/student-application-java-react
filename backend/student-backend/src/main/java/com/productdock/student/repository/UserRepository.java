package com.productdock.student.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.productdock.student.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
