package com.productdock.student.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import com.productdock.student.exception.ResourceNotFoundException;
import com.productdock.student.model.User;
import com.productdock.student.repository.UserRepository;


@CrossOrigin(origins="http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")

public class UserController {

	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/login")
	public List<User> getAllStudents(){
		return userRepository.findAll();
	}
	
}
