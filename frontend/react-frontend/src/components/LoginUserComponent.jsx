import React, { Component } from "react";
import userService from "../services/UserService";

class LoginUserComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      password: "",
    };
    this.changeUserNameHandler = this.changeUserNameHandler.bind(this);
    this.changePasswordHandler = this.changePasswordHandler.bind(this);
  }

  loginUser = (e) => {
    e.preventDefault();
    let user = { userName: this.state.userName, password: this.state.password }; //bojan password123 
    // console.log(user)    
    var passwordHash = this.stringToHash(user.password);
    var userData = userService.getUsers(); // not good practice to do this on frontend, do it on backend, uset HTTPS, you can send data and on backend make hash and compare values
    var dataJson = JSON.parse(userData.substring(1, userData.length - 1)); // not work if have 2 rows
    if (
      user.userName == dataJson.username &&
      passwordHash == dataJson.password
    ) {
      this.props.history.push("/add-student");
    } else {
    alert("Wrong username or password") // can count number of wrong atempts, can redirect...
    }
  };



  stringToHash(string) {
    var hash = 0;

    if (string.length == 0) return hash;

    for (var i = 0; i < string.length; i++) {
      var char = string.charCodeAt(i);
      hash = (hash << 5) - hash + char;
      hash = hash & hash;
    }

    return hash;
  }

  changeUserNameHandler = (event) => {
    this.setState({ userName: event.target.value });
  };
  changePasswordHandler = (event) => {
    this.setState({ password: event.target.value });
  };
  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="card col-md-6 offset-md-3 offset-md-3">
              <h3 className="text-center"> User Login</h3>
              <div className="card-body">
                <form>
                  <div className="form-group">
                    <label>Username: </label>
                    <input
                      placeholder="Username"
                      name="firstName"
                      className="form-control"
                      value={this.state.userName}
                      onChange={this.changeUserNameHandler}
                    />
                  </div>

                  <div className="form-group">
                    <label>Password: </label>
                    <input
                      type="password"
                      placeholder="Password"
                      name="lastName"
                      className="form-control"
                      value={this.state.password}
                      onChange={this.changePasswordHandler}
                    />
                  </div>
                  <button className="btn btn-success" onClick={this.loginUser}>
                    Login
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginUserComponent;
