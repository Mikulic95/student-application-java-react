import React, { Component } from "react";
import StudentService from "../services/StudentService";

class CreateStudentComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      firstName: "",
      lastName: "",
      indexNumber: "",
      averageGrade: "",
    };
    this.changeFirstNameHandler = this.changeFirstNameHandler.bind(this);
    this.changeLastNameHandler = this.changeLastNameHandler.bind(this);
    this.changeIndexNumberHandler = this.changeIndexNumberHandler.bind(this);
    this.changeAverageGradeHandler = this.changeAverageGradeHandler.bind(this);
    this.saveOrUpdateStudent = this.saveOrUpdateStudent.bind(this);
  }

  componentDidMount() {
    if (this.state.id === '_add') {
      return;
    } else {
      StudentService.getStudentById(this.state.id).then((res) => {
        let student = res.data;
        this.setState({
          firstName: student.firstName,
          lastName: student.lastName,
          indexNumber: student.indexNumber,
          averageGrade: student.averageGrade,
        });
      });
    }
  }

  saveOrUpdateStudent = (e) => {
    e.preventDefault();
    let student = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      indexNumber: this.state.indexNumber,
      averageGrade: this.state.averageGrade,
    };
    if (this.state.id === '_add') {
      StudentService.createStudent(student).then((res) => {
        this.props.history.push("/students");
      });
    } else {
      StudentService.updateStudent(student,this.state.id).then(res=>{
        this.props.history.push('/students');
    });
    }
   
  };

  getListOfStudents = (e) => {
    e.preventDefault();
    this.props.history.push("/students");
  };

  changeFirstNameHandler = (event) => {
    this.setState({ firstName: event.target.value });
  };

  changeLastNameHandler = (event) => {
    this.setState({ lastName: event.target.value });
  };
  changeIndexNumberHandler = (event) => {
    this.setState({ indexNumber: event.target.value });
  };
  changeAverageGradeHandler = (event) => {
    this.setState({ averageGrade: event.target.value });
  };

  cancel() {
    this.props.history.push("/students");
  }

  getTitle(){
    if(this.state.id === '_add'){
      return <h3 className="text-center"> Add Student</h3>
    } else{
      return <h3 className="text-center"> Update Student</h3>
    }
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="card col-md-6 offset-md-3 offset-md-3">
              {
                this.getTitle()
              }
              <div className="card-body">
                <form>
                  <div className="form-group">
                    <label>First Name: </label>
                    <input
                      placeholder="First Name"
                      name="firstName"
                      className="form-control"
                      value={this.state.firstName}
                      onChange={this.changeFirstNameHandler}
                    />
                  </div>

                  <div className="form-group">
                    <label>Last Name: </label>
                    <input
                      placeholder="Last Name"
                      name="lastName"
                      className="form-control"
                      value={this.state.lastName}
                      onChange={this.changeLastNameHandler}
                    />
                  </div>

                  <div className="form-group">
                    <label>Index Number: </label>
                    <input
                      placeholder="Index number"
                      name="indexNumber"
                      className="form-control"
                      value={this.state.indexNumber}
                      onChange={this.changeIndexNumberHandler}
                    />
                  </div>

                  <div className="form-group">
                    <label>Average Grade: </label>
                    <input
                      placeholder="Average Grade"
                      name="averageGrade"
                      className="form-control"
                      value={this.state.averageGrade}
                      onChange={this.changeAverageGradeHandler}
                    />
                  </div>

                  <button
                    className="btn btn-success"
                    onClick={this.saveOrUpdateStudent}
                  >
                    Save
                  </button>
                  <button
                    className="btn btn-danger"
                    onClick={this.cancel.bind(this)}
                    style={{ marginLeft: "10px" }}
                  >
                    Cancel
                  </button>

                  <button
                    className="btn btn-info"
                    onClick={this.getListOfStudents}
                    style={{ marginLeft: "10px" }}
                  >
                    See list of all students
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateStudentComponent;
