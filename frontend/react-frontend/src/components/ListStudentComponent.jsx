import React, { Component } from "react";
import StudentService from "../services/StudentService";
import studentService from "../services/StudentService";
import {withRouter} from "react-router-dom"

class ListStudentComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      students: [],
    };
    this.addStudent = this.addStudent.bind(this);
    this.editStudent = this.editStudent.bind(this)
    this.deleteStudent = this.deleteStudent.bind(this);
  }

  deleteStudent(id){
    StudentService.deleteStudent(id).then( res =>{
      this.setState({students: this.state.students.filter(student=>student.id !== id)});
    });
  }


  editStudent(id){
    this.props.history.push(`/add-student/${id}`);
  }


  componentDidMount() {
    studentService.getStudents().then((res) => {
      this.setState({ students: res.data });
    });
  }

  addStudent() {
    this.props.history.push("/add-student/_add");
  }

  render() {
    return (
      <div>
        <h2 className="text-center"> Students List </h2>

        <div className="row">
          <button className="btn btn-primary" onClick={this.addStudent}>
            Add Student
          </button>
        </div>

        <div className="row">
          <table className="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Student First Name</th>
                <th>Student Last Name</th>
                <th>Student Index Number</th>
                <th>Student Average Grade</th>
                <th>Actions</th>
              </tr>
            </thead>

            <tbody>
              {this.state.students.map((student) => (

                <tr key={student.id}>
                  <td>{student.firstName}</td>
                  <td>{student.lastName}</td>
                  <td>{student.indexNumber}</td>
                  <td>{student.averageGrade}</td>
                  <td>
                    <button onClick = {()=> this.editStudent(student.id)} className="btn btn-info">Update</button>
                    <button onClick = {()=> this.deleteStudent(student.id)} className="btn btn-danger" style={{marginLeft: "10px"}} >Delete </button>
                  </td>

                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default withRouter(ListStudentComponent);
