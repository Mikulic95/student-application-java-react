const USER_API_BASE_URL = "http://localhost:8080/api/v1/login";

class UserService {
  httpGet(url) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", url, false);
    xmlHttp.send(null);
    return xmlHttp.responseText;
  }

  getUsers() {
    return this.httpGet(USER_API_BASE_URL);
  }
}

export default new UserService();
