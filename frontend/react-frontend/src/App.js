
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HeaderComponent from "./components/HeaderComponent";
import FooterComponent from "./components/FooterComponent";
import ListStudentComponent from "./components/ListStudentComponent";
import CreateStudentComponent from "./components/CreateStudentComponent";
import LoginUserComponent from "./components/LoginUserComponent";


function App() {
  return (
    <div>
      <Router>

          <HeaderComponent />
          <div className="container">

            <Switch> 
              <Route path = "/" exact component = {LoginUserComponent}></Route>
              <Route path = "/login" exact component = {LoginUserComponent}></Route>
              <Route path = "/students" component = {ListStudentComponent}></Route>
              <Route path = "/add-student/:id" component = {CreateStudentComponent}></Route>              

              <ListStudentComponent />
            </Switch>
          </div>
          <FooterComponent />
        
      </Router>
    </div>
  );
}

export default App;
