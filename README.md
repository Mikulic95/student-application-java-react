# Student managment App

Basic CRUD application according to testing task.

# Run

Need to run MySQL server, database "student_managment" contains two tables "students" and "users". (Tested on localhost:3306)
Need to run backend of app written in Java. (Tested on localhost http://localhost:8080/api/v1/...) (run 'StudentApplication.java')
Need to run frontend of app written in React. (Tested on localhost http://localhost:3000/) (run npm start)


# Comment and description
```
This is basic CRUD app, when you run everything correct on http://localhost:3000/ you will get simple login form where you need to input your username and password. 
For this example, you only have one user, which data are stored in database student_managment in table users.
User credentials:
username: bojan  
password: password123

According to task implemented options: 
1. Button Add Student which you can add students in database.
2. Button for update data of every student.
3. Button for delete every student.
4. Showing all students happens when you login.
5. Filtering on some data implemented in options update and delete (id).

Connection to database should be for example in config file. But that isn't necessary for this type of project.
Login authentication should be implemented.
Try catch blocks for input fields for add/update student should be implemented.
